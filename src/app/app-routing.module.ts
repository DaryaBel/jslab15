import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPersonComponent } from './add-person/add-person.component';
import { ViewPersonComponent } from './view-person/view-person.component';
import { ListsComponent } from './lists/lists.component';

const routes: Routes = [
  {path: '', component: ListsComponent},
  {path: 'create', component: AddPersonComponent},
  {path: 'item/:id', component: ViewPersonComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
